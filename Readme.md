# FQPC

Input files for publication:  
M. Wozniak, E. Ravaioli and A. Verweij, "Fast Quench Propagation Conductor for Protecting Canted Cos-Theta Magnets," in IEEE Transactions on Applied Superconductivity, vol. 33, no. 5, pp. 1-5, Aug. 2023, Art no. 4701705, doi: https://doi.org/10.1109/TASC.2023.3247997  
(Copyright © 2023, CERN, Switzerland. All rights reserved.)

# Installation (Windows 64-bit only)
Obtain a copy of LEDET (version 2_02_16) executable by following instructions at https://espace.cern.ch/steam/_layouts/15/start.aspx#/LEDET/  
Download and unzip GetDP (version 3.5.0) executable folder: https://getdp.info/bin/Windows/getdp-3.5.0-Windows64r.zip  
Download and install Git for Windows (verion should not matter, for the paper 2.30.0.2 was used) from: https://git-scm.com/download/win  
Download and install Python (verion 3.8.0) from: https://www.python.org/ftp/python/3.8.0/python-3.8.0-amd64.exe  

git clone -b '0.0.6' --single-branch --depth 1 https://gitlab.cern.ch/steam/steam_models  
python pip install -r requirements.txt

# Editing and renaming user settings file
* Before running you need to edit settings.user.yaml. This can be done with a text editor of your choice.
* Edit paths to where you placed LEDET_v2_02_16.exe and getdp.exe by changing: C:\LEDET\LEDET_v2_02_16\LEDET_v2_02_16.exe and C:\getdp-3.5.0-Windows64\getdp.exe paths specified in the settings.username.yaml file.
* Edit paths to where you want the results to go by changing: C:\FQPC_FiQuS\ and C:\FQPC_LEDET\LEDET paths specified in the settings.username.yaml file.
* Rename the setting.username.yaml such that username is your user name Windows. For user name Bob, this is settings.Bob.yaml

# Choosing what to run
The repository files are configured to run all the simulations done for the paper. However, it might be more practical to run a few smaller subsets.  
These is why the simulations are split into batches for each magnet sizes.  
Magnet with 30 turns of the channel on each former is called M30 and the input file is called FQPC_30.yaml.  
You can choose to run only the FQPC_30.yaml by uncommenting (removing #) in front of the line file_names_analysis = ['FQPC_30'] in the analysis.py  

# Running the code
python analysis.py

# Links
STEAM website: https://espace.cern.ch/steam/

# Contact
steam-team@cern.ch

# STEAM User Agreement
By using any software of the STEAM framework, users agree with this document:
https://edms.cern.ch/document/2024516
