
# import sys
# local_sdk_path = r'E:\Python\steam_sdk'
# sys.path.insert(0, local_sdk_path)

from steam_sdk.analyses.AnalysisSTEAM import AnalysisSTEAM
import timeit
start_time = timeit.default_timer()
print(f'Start time: {start_time}')

if __name__ == "__main__":

    #file_names_analysis = ['MCBRD_FQPC_30', 'MCBRD_FQPC_100', 'MCBRD_FQPC_200']     # this runs everything done to get the results

    # simulations were started in parallel by uncommenting one lien below at the time and running this file.
    file_names_analysis = ['FQPC_30', 'FQPC_100', 'FQPC_200']
    #file_names_analysis = ['FQPC_30']
    #file_names_analysis = ['FQPC_100']
    #file_names_analysis = ['FQPC_200']
    #file_names_analysis = ['FQPC_365']     # extra input file. This was not used in the paper, as the temperatures get too high for a reasonably accurate simulation.

    for file_name_analysis in file_names_analysis:
        aSTEAM = AnalysisSTEAM(file_name_analysis=f'{file_name_analysis}.yaml', verbose=True)
        aSTEAM.run_analysis()

end_time = timeit.default_timer()
print(f'Run took {end_time - start_time:.2f} s')

print(f'End time: {end_time}')